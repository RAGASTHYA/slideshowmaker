package ssm.view;

import java.io.File;
import java.net.URL;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import properties_manager.PropertiesManager;
import ssm.LanguagePropertyType;
import static ssm.StartupConstants.CSS_CLASS_SLIDE_EDIT_VIEW;
import static ssm.StartupConstants.CSS_CLASS_SLIDE_EDIT_VIEW_HBOX;
import static ssm.StartupConstants.DEFAULT_THUMBNAIL_WIDTH;
import ssm.controller.ImageSelectionController;
import ssm.model.Slide;
import static ssm.file.SlideShowFileManager.SLASH;
import ssm.model.SlideShowModel;

/**
 * This UI component has the controls for editing a single slide in a slide
 * show, including controls for selected the slide image and changing its
 * caption.
 *
 * @author McKilla Gorilla & Rahul S Agasthya
 */
public class SlideEditView extends HBox {

    // SLIDE THIS COMPONENT EDITS
    Slide slide;
    SlideEditView selectedSlide;
    
    // DISPLAYS THE IMAGE FOR THIS SLIDE
    ImageView imageSelectionView;

    // CONTROLS FOR EDITING THE CAPTION
    VBox captionVBox;
    Label captionLabel;
    TextField captionTextField;

    // PROVIDES RESPONSES FOR IMAGE SELECTION
    ImageSelectionController imageController;

    /**
     * This constructor initializes the full UI for this component, using the
     * initSlide data for initializing values./
     *
     * @param initSlide The slide to be edited by this component.
     */
    public SlideEditView(Slide initSlide, SlideShowModel slideShow) {
        // FIRST SELECT THE CSS STYLE CLASS FOR THIS CONTAINER
        this.getStyleClass().add(CSS_CLASS_SLIDE_EDIT_VIEW);

        // KEEP THE SLIDE FOR LATER
        slide = initSlide;

        // MAKE SURE WE ARE DISPLAYING THE PROPER IMAGE
        imageSelectionView = new ImageView();
        updateSlideImage();

        // SETUP THE CAPTION CONTROLS
        captionVBox = new VBox();
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        captionLabel = new Label(props.getProperty(LanguagePropertyType.LABEL_CAPTION));
        captionTextField = new TextField();
        captionVBox.getChildren().add(captionLabel);
        captionVBox.getChildren().add(captionTextField);

        // LAY EVERYTHING OUT INSIDE THIS COMPONENT
        getChildren().add(imageSelectionView);
        getChildren().add(captionVBox);

        // SETUP THE EVENT HANDLERS
        imageController = new ImageSelectionController();
        
        if (slide.getCaption() != null) {
            captionTextField.setText(slide.getCaption());
        }
        if (!slide.getCaption().equals(captionTextField.getText())) {
            slideShow.setEdited(true);
        }
        captionTextField.textProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
                slide.setCaption(newValue);
            }
        });

        //SETS THE STYLE FOR THE SLIDE WHICH IS SELECTED.
        this.setOnMousePressed(e -> {
            if (selectedSlide != null) {
                selectedSlide.setStyle("-fx-border-style: solid;" + "fx-border-width: 0px;" + "fx-border-color: null");
                selectedSlide.slide.setSelected(false);
            }
            slideShow.setSelectedSlide(this.slide);
            selectedSlide = new SlideEditView(this.slide, slideShow);
            this.slide.setSelected(true);
            if (selectedSlide.slide.isSelected()) 
                this.setStyle("-fx-border-style: solid;" + "fx-border-width: 15px;" + "fx-border-color: blue");
            imageSelectionView.setOnMousePressed(f -> {
                imageController.processSelectImage(slide, this);
            });
        });

    }

    /**
     * This function gets the image for the slide and uses it to update the
     * image displayed.
     */
    public void updateSlideImage() {
        String imagePath = slide.getImagePath() + SLASH + slide.getImageFileName();
        File file = new File(imagePath);
        try {
            // GET AND SET THE IMAGE
            URL fileURL = file.toURI().toURL();
            Image slideImage = new Image(fileURL.toExternalForm());
            imageSelectionView.setImage(slideImage);

            // AND RESIZE IT
            double scaledWidth = DEFAULT_THUMBNAIL_WIDTH;
            double perc = scaledWidth / slideImage.getWidth();
            double scaledHeight = slideImage.getHeight() * perc;
            imageSelectionView.setFitWidth(scaledWidth);
            imageSelectionView.setFitHeight(scaledHeight);
        } catch (Exception e) {
            Stage invalidImage = new Stage();
            BorderPane pane = new BorderPane();
            HBox message = new HBox();
            ImageView image = new ImageView(new Image("file:./images/icons/InvalidImage.png"));
            Text t = new Text();
            t.setText("ERROR 404 : Image Not Found.");
            t.setFont(Font.font("Verdana", 25));
            message.getChildren().addAll(image, t);
            message.setAlignment(Pos.CENTER);
            pane.setCenter(message);

            Button OK = new Button("OK");
            pane.setBottom(OK);

            invalidImage.setScene(new Scene(pane));
            invalidImage.show();

            OK.setOnAction(f -> {
                imageController.processSelectImage(slide, this);
            });
        }
    }
}
