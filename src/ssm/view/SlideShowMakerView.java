package ssm.view;

import java.io.File;
import java.net.URL;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.geometry.Rectangle2D;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleGroup;
import javafx.scene.control.Tooltip;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import static javafx.scene.paint.Color.*;
import javafx.scene.text.Font;
import static javafx.scene.text.FontWeight.BOLD;
import javafx.scene.text.Text;
import javafx.scene.text.TextAlignment;
import javafx.stage.Screen;
import javafx.stage.Stage;
import properties_manager.PropertiesManager;
import ssm.LanguagePropertyType;
import static ssm.LanguagePropertyType.DEFAULT_SLIDE_SHOW_TITLE;
import static ssm.LanguagePropertyType.TOOLTIP_ADD_SLIDE;
import static ssm.LanguagePropertyType.TOOLTIP_EXIT;
import static ssm.LanguagePropertyType.TOOLTIP_EXIT_FULL_SCREEN;
import static ssm.LanguagePropertyType.TOOLTIP_EXIT_SHOW;
import static ssm.LanguagePropertyType.TOOLTIP_FULL_SCREEN;
import static ssm.LanguagePropertyType.TOOLTIP_LOAD_SLIDE_SHOW;
import static ssm.LanguagePropertyType.TOOLTIP_MOVE_DOWN;
import static ssm.LanguagePropertyType.TOOLTIP_MOVE_UP;
import static ssm.LanguagePropertyType.TOOLTIP_NEW_SLIDE_SHOW;
import static ssm.LanguagePropertyType.TOOLTIP_NEXT_SLIDE;
import static ssm.LanguagePropertyType.TOOLTIP_PREVIOUS_SLIDE;
import static ssm.LanguagePropertyType.TOOLTIP_REMOVE_SLIDE;
import static ssm.LanguagePropertyType.TOOLTIP_RENAME;
import static ssm.LanguagePropertyType.TOOLTIP_SAVE_SLIDE_SHOW;
import static ssm.LanguagePropertyType.TOOLTIP_VIEW_SLIDE_SHOW;
import ssm.StartupConstants;
import static ssm.StartupConstants.CSS_CLASS_HORIZONTAL_TOOLBAR_BUTTON;
import static ssm.StartupConstants.CSS_CLASS_SLIDE_EDIT_VIEW_HBOX;
import static ssm.StartupConstants.CSS_CLASS_SLIDE_SHOW_EDIT_VBOX;
import static ssm.StartupConstants.CSS_CLASS_VERTICAL_TOOLBAR_BUTTON;
import static ssm.StartupConstants.DEFAULT_SLIDE_IMAGE;
import static ssm.StartupConstants.ICON_ADD_SLIDE;
import static ssm.StartupConstants.ICON_EXIT;
import static ssm.StartupConstants.ICON_EXIT_FULL_SCREEN;
import static ssm.StartupConstants.ICON_EXIT_SHOW;
import static ssm.StartupConstants.ICON_FULL_SCREEN;
import static ssm.StartupConstants.ICON_LOAD_SLIDE_SHOW;
import static ssm.StartupConstants.ICON_MOVE_DOWN;
import static ssm.StartupConstants.ICON_MOVE_UP;
import static ssm.StartupConstants.ICON_NEW_SLIDE_SHOW;
import static ssm.StartupConstants.ICON_NEXT;
import static ssm.StartupConstants.ICON_PREVIOUS;
import static ssm.StartupConstants.ICON_REMOVE_SLIDE;
import static ssm.StartupConstants.ICON_RENAME_FILE;
import static ssm.StartupConstants.ICON_SAVE_SLIDE_SHOW;
import static ssm.StartupConstants.ICON_VIEW_SLIDE_SHOW;
import static ssm.StartupConstants.PATH_ICONS;
import static ssm.StartupConstants.STYLE_SHEET_UI;
import ssm.controller.FileController;
import ssm.controller.SlideShowEditController;
import ssm.model.Slide;
import ssm.model.SlideShowModel;
import ssm.error.ErrorHandler;
import ssm.file.SlideShowFileManager;
import static ssm.file.SlideShowFileManager.SLASH;

/**
 * This class provides the User Interface for this application, providing
 * controls and the entry points for creating, loading, saving, editing, and
 * viewing slide shows.
 *
 * @author McKilla Gorilla & Rahul S Agasthya
 */
public class SlideShowMakerView {

    // THIS IS THE MAIN APPLICATION UI WINDOW AND ITS SCENE GRAPH
    Stage primaryStage;
    Scene primaryScene;

    // THIS PANE ORGANIZES THE BIG PICTURE CONTAINERS FOR THE
    // APPLICATION GUI
    BorderPane ssmPane;

    // THIS IS THE TOP TOOLBAR AND ITS CONTROLS
    FlowPane fileToolbarPane;
    Button newSlideShowButton;
    Button loadSlideShowButton;
    Button saveSlideShowButton;
    Button viewSlideShowButton;
    Button renameTitle;
    Button exitButton;

    // WORKSPACE
    HBox workspace;

    // THIS WILL GO IN THE LEFT SIDE OF THE SCREEN
    VBox slideEditToolbar;
    Button addSlideButton;
    Button moveUpButton;
    Button moveDownButton;
    Button removeSlideButton;

    // AND THIS WILL GO IN THE CENTER
    ScrollPane slidesEditorScrollPane;
    VBox slidesEditorPane;

    // THIS IS THE SLIDE SHOW WE'RE WORKING WITH
    SlideShowModel slideShow;

    //THIS IS THE SLIDE THE USER SELECTS
    Slide selectSlide;
    
    //CHECKS WHETHER THE SLIDE SHOW IS SAVED
    boolean saved;

    // THIS IS FOR SAVING AND LOADING SLIDE SHOWS
    SlideShowFileManager fileManager;

    // THIS CLASS WILL HANDLE ALL ERRORS FOR THIS PROGRAM
    private ErrorHandler errorHandler;

    // THIS CONTROLLER WILL ROUTE THE PROPER RESPONSES
    // ASSOCIATED WITH THE FILE TOOLBAR
    private FileController fileController;

    // THIS CONTROLLER RESPONDS TO SLIDE SHOW EDIT BUTTONS
    private SlideShowEditController editController;

    //KEEPS TRACK OF THE INDEX WHILE PLAYING THE SLIDE SHOW
    int start;
    
    /**
     * Default constructor, it initializes the GUI for use, but does not yet
     * load all the language-dependent controls, that needs to be done via the
     * startUI method after the user has selected a language.
     */
    public SlideShowMakerView(SlideShowFileManager initFileManager) {
        // FIRST HOLD ONTO THE FILE MANAGER
        fileManager = initFileManager;
        // MAKE THE DATA MANAGING MODEL
        slideShow = new SlideShowModel(this);

        // WE'LL USE THIS ERROR HANDLER WHEN SOMETHING GOES WRONG
        errorHandler = new ErrorHandler(this);
    }

    // ACCESSOR METHODS
    public SlideShowModel getSlideShow() {
        return slideShow;
    }

    public Stage getWindow() {
        return primaryStage;
    }

    public ErrorHandler getErrorHandler() {
        return errorHandler;
    }

    /**
     * Initializes the UI controls and gets it rolling.
     *
     * @param initPrimaryStage The window for this application.
     *
     * @param windowTitle The title for this window.
     */
    public void startUI(Stage initPrimaryStage, String windowTitle) {
        // THE TOOLBAR ALONG THE NORTH
        initFileToolbar();

        // INIT THE CENTER WORKSPACE CONTROLS BUT DON'T ADD THEM
        // TO THE WINDOW YET
        initWorkspace();

        // NOW SETUP THE EVENT HANDLERS
        initEventHandlers();

        // AND FINALLY START UP THE WINDOW (WITHOUT THE WORKSPACE)
        // KEEP THE WINDOW FOR LATER
        primaryStage = initPrimaryStage;
        initWindow(windowTitle);
    }

    // UI SETUP HELPER METHODS
    private void initWorkspace() {
        // FIRST THE WORKSPACE ITSELF, WHICH WILL CONTAIN TWO REGIONS
        workspace = new HBox();

        // THIS WILL GO IN THE LEFT SIDE OF THE SCREEN
        slideEditToolbar = new VBox();
        slideEditToolbar.getStyleClass().add(CSS_CLASS_SLIDE_SHOW_EDIT_VBOX);
        addSlideButton = this.initChildButton(slideEditToolbar, ICON_ADD_SLIDE, TOOLTIP_ADD_SLIDE, CSS_CLASS_VERTICAL_TOOLBAR_BUTTON, true);
        moveUpButton = this.initChildButton(slideEditToolbar, ICON_MOVE_UP, TOOLTIP_MOVE_UP, CSS_CLASS_VERTICAL_TOOLBAR_BUTTON, (slideShow.size() >= 2) ? false : true);
        moveDownButton = this.initChildButton(slideEditToolbar, ICON_MOVE_DOWN, TOOLTIP_MOVE_DOWN, CSS_CLASS_VERTICAL_TOOLBAR_BUTTON, (slideShow.size() >= 2) ? false : true);
        removeSlideButton = this.initChildButton(slideEditToolbar, ICON_REMOVE_SLIDE, TOOLTIP_REMOVE_SLIDE, CSS_CLASS_VERTICAL_TOOLBAR_BUTTON, (slideShow.size() >= 1) ? false : true);

        // AND THIS WILL GO IN THE CENTER
        slidesEditorPane = new VBox();
        slidesEditorScrollPane = new ScrollPane(slidesEditorPane);

        // NOW PUT THESE TWO IN THE WORKSPACE
        workspace.getChildren().add(slideEditToolbar);
        workspace.getChildren().add(slidesEditorScrollPane);
    }

    private void initEventHandlers() {
        // FIRST THE FILE CONTROLS
        fileController = new FileController(this, fileManager);
        
        /**
         * When the user pushes the create new slide show button,
         * a dialog box will pop open to accept the user's preferred language.
         */
        newSlideShowButton.setOnAction(e -> {
            Stage title_stage = new Stage();
            TextField title = new TextField();
            title.setMaxSize(200, 50);
            PropertiesManager props = PropertiesManager.getPropertiesManager();
            Tooltip titleToolTip = new Tooltip(props.getProperty(DEFAULT_SLIDE_SHOW_TITLE.toString()));
            Label title_label = new Label(titleToolTip.getText());
            title_label.setFont(Font.font("Verdana", 20));
            title_label.setAlignment(Pos.CENTER);
            Button create_new = new Button("CREATE SLIDE SHOW");
            VBox title_pane = new VBox();
            title_pane.getChildren().addAll(title_label, title, create_new);
            BorderPane pane = new BorderPane();
            pane.setCenter(title_pane);
            Text alert = new Text();
            alert.setText("Please Enter the Title of the Slide Show.");
            alert.setFont(Font.font("Verdana", 15));
            alert.setFill(RED);
            alert.setTextAlignment(TextAlignment.CENTER);
            title_stage.setMinHeight(300);
            title_stage.setMinWidth(500);
            title_stage.setScene(new Scene(pane));
            title_stage.show();
            create_new.setOnAction(e1 -> {
                if (title.getText().isEmpty()) {
                    pane.setBottom(alert);
                } else {
                    fileController.handleNewSlideShowRequest();
                    slideShow.setTitle(title.getText());
                    slideShow.setEdited(false);
                    reloadSlideShowPane(slideShow);
                    title_stage.close();

                }
            });

        });
        
        /**
         * This is the action for the Load Slide Show Button.
         * 
         * This will read the JSON file saved previously.
         */
        loadSlideShowButton.setOnAction(e -> {
            fileController.handleLoadSlideShowRequest();
            saveSlideShowButton.setDisable(true);
            reloadSlideShowPane(slideShow);
            saved = true;
            if (slideShow.size() >= 2) {
                moveUpButton.setDisable(false);
                moveDownButton.setDisable(false);
            }

            if (slideShow.size() >= 1) {
                removeSlideButton.setDisable(false);
                viewSlideShowButton.setDisable(false);
            }

        });
        
        /**
         * This will save the slide show as a JSON file.
         */
        saveSlideShowButton.setOnAction(e -> {
            saveSlideShowButton.setDisable(true);
            saved = true;
            slideShow.setEdited(false);
            fileController.handleSaveSlideShowRequest();
        });
        
        /**
         * This is used to view the slide show.
         * 
         * A new stage will pop out with the title, image and the image caption.
         * 
         * One can move next, previous, exit and view in full screen.
         */
        viewSlideShowButton.setOnAction(e -> {
            //Counts the number of blank slides i.e. without any image.
            int count = 0;
            for (int i = 0; i < slideShow.size(); i++) {
                if (slideShow.getSlides().get(i).getImageFileName().equals(DEFAULT_SLIDE_IMAGE)) {
                    count++;
                }
            }
            
            //CREATES A STAGE FOR THE SLIDE SHOW.
            Stage slide_show_view = new Stage();
            BorderPane pane = new BorderPane();
            VBox control_panel = new VBox();
            VBox imageDetails = new VBox();
            imageDetails.setAlignment(Pos.CENTER);
            Text caption = new Text();
            caption.setFont(Font.font("Californian FB", 25));
            caption.setTextAlignment(TextAlignment.CENTER);
            
            //EXTRACTS THE TITLE OF THE SLDIE SHOW.
            Text title = new Text(slideShow.getTitle());
            title.setFont(Font.font("Californian FB", BOLD, 35));
            title.setTextAlignment(TextAlignment.CENTER);
            pane.setTop(title);
            
            //CREATES THE CONTROL BUTTONS
            Button moveNext = this.initChildButton(control_panel, ICON_NEXT, TOOLTIP_NEXT_SLIDE, CSS_CLASS_VERTICAL_TOOLBAR_BUTTON, false);
            Button movePrev = this.initChildButton(control_panel, ICON_PREVIOUS, TOOLTIP_PREVIOUS_SLIDE, CSS_CLASS_VERTICAL_TOOLBAR_BUTTON, false);
            Button exitShow = this.initChildButton(control_panel, ICON_EXIT_SHOW, TOOLTIP_EXIT_SHOW, CSS_CLASS_VERTICAL_TOOLBAR_BUTTON, false);
            Button fullScreen = this.initChildButton(control_panel, ICON_FULL_SCREEN, TOOLTIP_FULL_SCREEN, CSS_CLASS_VERTICAL_TOOLBAR_BUTTON, false);
            Button exitFullScreen = this.initChildButton(control_panel, ICON_EXIT_FULL_SCREEN, TOOLTIP_EXIT_FULL_SCREEN, CSS_CLASS_VERTICAL_TOOLBAR_BUTTON, true);
            exitFullScreen.setVisible(false);
            //SETS THE PANE
            pane.setRight(control_panel);
            slide_show_view.setScene(new Scene(pane));
            slide_show_view.setWidth(1920);
            slide_show_view.setHeight(1080);
            
            //DISPLAYS ERROR MESSAGE IF ANY SLIDE HAS NO IMAGE.
            if (count == 0) {
                slide_show_view.show();
            } else {
                Stage noImage = new Stage();
                noImage.setHeight(270);
                noImage.setWidth(750);
                BorderPane errorPane = new BorderPane();
                HBox message = new HBox();
                ImageView image = new ImageView(new Image("file:./images/icons/InvalidImage.png"));
                Text t = new Text();
                t.setText("ERROR 404 : Image Not Found.");
                t.setFont(Font.font("Verdana", 25));
                t.setFill(RED);
                message.getChildren().addAll(image, t);
                message.setAlignment(Pos.CENTER);
                errorPane.setCenter(message);

                Button OKButton = new Button("OK");
                OKButton.setAlignment(Pos.CENTER);
                errorPane.setBottom(OKButton);
                noImage.setScene(new Scene(errorPane));
                noImage.show();

                OKButton.setOnAction(f -> {
                    noImage.close();
                });
            }
            
            //STARTS THE SLIDE SHOW.
            start = 0;
            String imagePath = slideShow.getSlides().get(start).getImagePath() + SLASH + slideShow.getSlides().get(start).getImageFileName();
            File file = new File(imagePath);
            Image slideImage = new Image(file.toURI().toString());
            ImageView image = new ImageView(slideImage);
            caption.setText(slideShow.getSlides().get(start).getCaption());
            imageDetails.getChildren().add(image);
            imageDetails.getChildren().add(caption);
            pane.setCenter(imageDetails);
            moveNext.setOnAction(e1 -> {
                //MOVES NEXT, IF THE COUNTER BUMPS INTO THE LAST SLIDE, MOVES TO SLIDE 1.
                start++;
                start = start % slideShow.size();
                String imagePathNext = slideShow.getSlides().get(start).getImagePath() + SLASH + slideShow.getSlides().get(start).getImageFileName();
                File fileNext = new File(imagePathNext);
                Image slideImageNext = new Image(fileNext.toURI().toString());
                ImageView imageNext = new ImageView(slideImageNext);
                imageNext.setFitHeight(588);
                imageNext.setFitWidth(892);
                VBox imageDetailsNext = new VBox();
                imageDetailsNext.setAlignment(Pos.CENTER);
                imageDetailsNext.getChildren().add(imageNext);
                caption.setText(slideShow.getSlides().get(start).getCaption());
                imageDetailsNext.getChildren().add(caption);
                pane.setCenter(imageDetailsNext);
            });
            movePrev.setOnAction(e1 -> {
                //MOVES PREVIOUS, IF THE COUNTER BUMPS INTO THE FIRST SLIDE, MOVES TO THE LAST SLIDE.
                start = start - 1;
                if (start < 0) {
                    start = slideShow.size() - 1;
                }
                start = start % slideShow.size();
                String imagePathPrev = slideShow.getSlides().get(start).getImagePath() + SLASH + slideShow.getSlides().get(start).getImageFileName();
                File filePrev = new File(imagePathPrev);
                Image slideImagePrev = new Image(filePrev.toURI().toString());
                ImageView imagePrev = new ImageView(slideImagePrev);
                imagePrev.setFitHeight(588);
                imagePrev.setFitWidth(892);
                VBox imageDetailsPrev = new VBox();
                imageDetailsPrev.setAlignment(Pos.CENTER);
                imageDetailsPrev.getChildren().add(imagePrev);
                caption.setText(slideShow.getSlides().get(start).getCaption());
                imageDetailsPrev.getChildren().add(caption);
                pane.setCenter(imageDetailsPrev);
            });
            
            //OPENS THE STAGE IN FULL SCREEN
            fullScreen.setOnAction(e1 -> {
                slide_show_view.setFullScreen(true);
                fullScreen.setVisible(false);
                exitFullScreen.setVisible(true);
                exitFullScreen.setDisable(false);
            });
            
            //EXIT FULL SCREEN
            exitFullScreen.setOnAction(e1 -> {
                slide_show_view.setFullScreen(false);
                fullScreen.setVisible(true);
                exitFullScreen.setVisible(false);
                exitFullScreen.setDisable(true);
            });
            exitShow.setOnAction(e1 -> slide_show_view.close());

        });
        
        /**
         * Exits the slide show maker application.
         * 
         * Prompts the user to save the slide show.
         */
        exitButton.setOnAction(e -> {
            Stage exitWindow = new Stage();
            BorderPane pane = new BorderPane();
            HBox buttonsPane = new HBox();
            Text t = new Text();
            t.setText("Do you want to save changes to " + slideShow.getTitle());
            t.setFont(Font.font("Verdana", 20));
            t.setFill(Color.RED);
            pane.setCenter(t);

            Button saveButton = new Button("SAVE AND EXIT");
            Button exitButton = new Button("EXIT WITHOUT SAVING");
            Button cancelButton = new Button("RETURN TO WORKSPACE");

            buttonsPane.setPadding(new Insets(10, 30, 10, 30));

            buttonsPane.getChildren().addAll(saveButton, exitButton, cancelButton);
            buttonsPane.setAlignment(Pos.CENTER);
            pane.setBottom(buttonsPane);
            exitWindow.setScene(new Scene(pane));
            fileController.setSaved(!slideShow.isEdited());
            if (slideShow.size() == 0) {
                primaryStage.close();
            } else {
                if (!saved) {
                    exitWindow.show();
                } else {
                    primaryStage.close();
                }
            }
            
            //SAVE AND EXIT THE SLIDE SHOW MAKER APPLICATION.
            saveButton.setOnAction(f -> {
                fileController.handleSaveSlideShowRequest();
                fileController.handleExitRequest(true);
                exitWindow.close();
                primaryStage.close();
            });

            //EXIT THE SLIDE SHOW MAKER APPLICATION WITHOUT SAVING.
            exitButton.setOnAction(f -> {
                fileController.handleExitRequest(false);
                exitWindow.close();
            });

            //GOES BACK TO SLIDE SHOW MAKER APPLICATION.
            cancelButton.setOnAction(f -> {
                exitWindow.close();
            });
        });

        renameTitle.setOnAction(e -> {
            Stage title_stage = new Stage();
            TextField title = new TextField();
            title.setMaxSize(200, 50);
            PropertiesManager props = PropertiesManager.getPropertiesManager();
            Tooltip titleToolTip = new Tooltip(props.getProperty(DEFAULT_SLIDE_SHOW_TITLE.toString()));
            Label title_label = new Label(titleToolTip.getText());
            title_label.setFont(Font.font("Verdana", 20));
            title_label.setAlignment(Pos.CENTER);
            Button create_new = new Button("RENAME SLIDE SHOW");
            VBox title_pane = new VBox();
            title_pane.getChildren().addAll(title_label, title, create_new);
            BorderPane pane = new BorderPane();
            pane.setCenter(title_pane);
            Text alert = new Text();
            alert.setText("Please Enter the Title of the Slide Show.");
            alert.setFont(Font.font("Verdana", 15));
            alert.setFill(RED);
            alert.setTextAlignment(TextAlignment.CENTER);
            title_stage.setMinHeight(300);
            title_stage.setMinWidth(500);
            title_stage.setScene(new Scene(pane));
            title_stage.show();
            create_new.setOnAction(e1 -> {
                if (title.getText().isEmpty()) {
                    pane.setBottom(alert);
                } else {
                    slideShow.setTitle(title.getText());
                    reloadSlideShowPane(slideShow);
                    title_stage.close();

                }
            });
        });
        // THEN THE SLIDE SHOW EDIT CONTROLS
        editController = new SlideShowEditController(this);
        
        //ADDS SLIDE.
        addSlideButton.setOnAction(e -> {
            editController.processAddSlideRequest();
            saveSlideShowButton.setDisable(false);
            fileController.markAsEdited();
            slideShow.setEdited(false);
            saved = false;
            if (slideShow.size() >= 2) {
                moveUpButton.setDisable(false);
                moveDownButton.setDisable(false);
            }

            if (slideShow.size() >= 1) {
                removeSlideButton.setDisable(false);
                viewSlideShowButton.setDisable(false);
            }
        });

        //REMOVES A SLIDE.
        removeSlideButton.setOnAction(e -> {
            editController.processRemoveRequest();
            saveSlideShowButton.setDisable(false);
            slideShow.setEdited(false);
            fileController.markAsEdited();
            saved = false;
            if (slideShow.size() < 2) {
                moveUpButton.setDisable(true);
                moveDownButton.setDisable(true);
            }

            if (slideShow.size() < 1) {
                removeSlideButton.setDisable(true);
                viewSlideShowButton.setDisable(true);
            }
        });

        //MOVE UP A SLIDE.
        moveUpButton.setOnAction(e -> {
            editController.processMoveUpRequest();
            saveSlideShowButton.setDisable(false);
            slideShow.setEdited(false);
            fileController.markAsEdited();
            saved = false;
        });

        //MOVE DOWN A SLIDE.
        moveDownButton.setOnAction(e -> {
            editController.processMoveDownRequest();
            saveSlideShowButton.setDisable(false);
            slideShow.setEdited(false);
            fileController.markAsEdited();
            saved = false;
        });
    }

    /**
     * This function initializes all the buttons in the toolbar at the top of
     * the application window. These are related to file management.
     */
    private void initFileToolbar() {
        fileToolbarPane = new FlowPane();

        // HERE ARE OUR FILE TOOLBAR BUTTONS, NOTE THAT SOME WILL
        // START AS ENABLED (false), WHILE OTHERS DISABLED (true)
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        newSlideShowButton = initChildButton(fileToolbarPane, ICON_NEW_SLIDE_SHOW, TOOLTIP_NEW_SLIDE_SHOW, CSS_CLASS_HORIZONTAL_TOOLBAR_BUTTON, false);
        loadSlideShowButton = initChildButton(fileToolbarPane, ICON_LOAD_SLIDE_SHOW, TOOLTIP_LOAD_SLIDE_SHOW, CSS_CLASS_HORIZONTAL_TOOLBAR_BUTTON, false);
        saveSlideShowButton = initChildButton(fileToolbarPane, ICON_SAVE_SLIDE_SHOW, TOOLTIP_SAVE_SLIDE_SHOW, CSS_CLASS_HORIZONTAL_TOOLBAR_BUTTON, true);
        viewSlideShowButton = initChildButton(fileToolbarPane, ICON_VIEW_SLIDE_SHOW, TOOLTIP_VIEW_SLIDE_SHOW, CSS_CLASS_HORIZONTAL_TOOLBAR_BUTTON, true);
        renameTitle = initChildButton(fileToolbarPane, ICON_RENAME_FILE, TOOLTIP_RENAME, CSS_CLASS_HORIZONTAL_TOOLBAR_BUTTON, false);
        exitButton = initChildButton(fileToolbarPane, ICON_EXIT, TOOLTIP_EXIT, CSS_CLASS_HORIZONTAL_TOOLBAR_BUTTON, false);
    }

    private void initWindow(String windowTitle) {
        // SET THE WINDOW TITLE
        primaryStage.setTitle(windowTitle);
        primaryStage.getIcons().add(new Image("file:./images/icons/projectIcon.png"));

        // GET THE SIZE OF THE SCREEN
        Screen screen = Screen.getPrimary();
        Rectangle2D bounds = screen.getVisualBounds();

        // AND USE IT TO SIZE THE WINDOW
        primaryStage.setX(bounds.getMinX());
        primaryStage.setY(bounds.getMinY());
        primaryStage.setWidth(bounds.getWidth());
        primaryStage.setHeight(bounds.getHeight());

        // SETUP THE UI, NOTE WE'LL ADD THE WORKSPACE LATER
        ssmPane = new BorderPane();
        ssmPane.setTop(fileToolbarPane);
        primaryScene = new Scene(ssmPane);

        // NOW TIE THE SCENE TO THE WINDOW, SELECT THE STYLESHEET
        // WE'LL USE TO STYLIZE OUR GUI CONTROLS, AND OPEN THE WINDOW
        primaryScene.getStylesheets().add(STYLE_SHEET_UI);
        primaryStage.setScene(primaryScene);
        primaryStage.show();
    }

    /**
     * This helps initialize buttons in a toolbar, constructing a custom button
     * with a customly provided icon and tooltip, adding it to the provided
     * toolbar pane, and then returning it.
     */
    public Button initChildButton(
            Pane toolbar,
            String iconFileName,
            LanguagePropertyType tooltip,
            String cssClass,
            boolean disabled) {
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        String imagePath = "file:" + PATH_ICONS + iconFileName;
        Image buttonImage = new Image(imagePath);
        Button button = new Button();
        button.getStyleClass().add(cssClass);
        button.setDisable(disabled);
        button.setGraphic(new ImageView(buttonImage));
        Tooltip buttonTooltip = new Tooltip(props.getProperty(tooltip.toString()));
        button.setTooltip(buttonTooltip);
        toolbar.getChildren().add(button);
        return button;
    }

    /**
     * Updates the enabled/disabled status of all toolbar buttons.
     *
     * @param saved
     */
    public void updateToolbarControls(boolean saved) {
        // FIRST MAKE SURE THE WORKSPACE IS THERE
        ssmPane.setCenter(workspace);

        // NEXT ENABLE/DISABLE BUTTONS AS NEEDED IN THE FILE TOOLBAR
        saveSlideShowButton.setDisable(saved);
        if (slideShow.isEdited()) {
            this.saved = false;
        }
        if (!this.saved) {
            saveSlideShowButton.setDisable(false);
        }

        if (slideShow.size() >= 1) {
            viewSlideShowButton.setDisable(false);
        } else {
            viewSlideShowButton.setDisable(true);
        }

        // AND THE SLIDESHOW EDIT TOOLBAR
        addSlideButton.setDisable(false);
    }

    /**
     * Uses the slide show data to reload all the components for slide editing.
     *
     * @param slideShowToLoad SLide show being reloaded.
     */
    public void reloadSlideShowPane(SlideShowModel slideShowToLoad) {
        slidesEditorPane.getChildren().clear();
        for (Slide slide : slideShowToLoad.getSlides()) {
            SlideEditView slideEditor = new SlideEditView(slide, this.slideShow);
            slideEditor.getStyleClass().add(CSS_CLASS_SLIDE_EDIT_VIEW_HBOX);
            slidesEditorPane.getChildren().add(slideEditor);
        }
    }
}
