package ssm;

import javafx.application.Application;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.RadioButton;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleGroup;
import javafx.scene.control.Tooltip;
import javafx.scene.image.Image;
import static javafx.scene.input.KeyCode.C;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.scene.text.TextAlignment;
import javafx.stage.Stage;
import xml_utilities.InvalidXMLFileFormatException;
import properties_manager.PropertiesManager;
import static ssm.LanguagePropertyType.DEFAULT_SLIDE_SHOW_TITLE;
import static ssm.LanguagePropertyType.TITLE_WINDOW;
import static ssm.StartupConstants.PATH_DATA;
import static ssm.StartupConstants.PROPERTIES_SCHEMA_FILE_NAME;
import static ssm.StartupConstants.UI_PROPERTIES_FILE_NAME;
import ssm.error.ErrorHandler;
import ssm.file.SlideShowFileManager;
import ssm.model.SlideShowModel;
import ssm.view.SlideShowMakerView;

/**
 * SlideShowMaker is a program for making custom image slideshows. It will allow
 * the user to name their slideshow, select images to use, select captions for
 * the images, and the order of appearance for slides.
 *
 * @author McKilla Gorilla & Rahul S Agasthya
 */
public class SlideShowMaker extends Application {

    // THIS WILL PERFORM SLIDESHOW READING AND WRITING
    SlideShowFileManager fileManager = new SlideShowFileManager();

    //LANGUAGE INPUT.
    String[] languages = {"properties_EN.xml", "properties_FR.xml", "properties_HD.xml"};
    Stage languageInput;

    // THIS HAS THE FULL USER INTERFACE AND ONCE IN EVENT
    // HANDLING MODE, BASICALLY IT BECOMES THE FOCAL
    // POINT, RUNNING THE UI AND EVERYTHING ELSE
    SlideShowMakerView ui = new SlideShowMakerView(fileManager);

    @Override
    public void start(Stage primaryStage) throws Exception {
        // LOAD APP SETTINGS INTO THE GUI AND START IT UP

        //THE LANGUAGE SELECTION STAGE.
        languageInput = new Stage();
        languageInput.getIcons().add(new Image("file:./images/icons/projectIcon.png"));
        languageInput.setTitle("Language Settings");
        BorderPane pane = new BorderPane();
        VBox language = new VBox();
        VBox titles = new VBox();
        Text label = new Text("Select a Language");
        label.setFont(Font.font("Verdana", 25));
        ChoiceBox cb = new ChoiceBox(FXCollections.observableArrayList("ENGLISH", "FRANCIS", "हिंदी"));
        cb.setValue("ENGLISH");
        language.getChildren().add(cb);
        language.setPadding(new Insets(10, 10, 10, 10));
        language.setAlignment(Pos.CENTER);
        label.setTextAlignment(TextAlignment.CENTER);
        pane.setTop(label);
        pane.setCenter(language);
        Button done = new Button("DONE!!");
        done.setAlignment(Pos.CENTER);
        pane.setBottom(done);
        cb.setTooltip(new Tooltip("Select the language"));
        cb.getSelectionModel().selectedIndexProperty().addListener(new ChangeListener<Number>() {
            public void changed(ObservableValue ov, Number value, Number new_value) {
                StartupConstants.UI_PROPERTIES_FILE_NAME = languages[new_value.intValue()];
            }
        });
        done.setOnAction(e -> {
            //SETS THE LANGUAGE FOR THE APPLICATION.
            languageInput.close();
            boolean success = loadProperties();
            if (success) {
                PropertiesManager props = PropertiesManager.getPropertiesManager();
                String appTitle = props.getProperty(TITLE_WINDOW);

                // NOW START THE UI IN EVENT HANDLING MODE
                ui.startUI(primaryStage, appTitle);
            } // THERE WAS A PROBLEM LOADING THE PROPERTIES FILE
            else {
                // LET THE ERROR HANDLER PROVIDE THE RESPONSE
                ErrorHandler errorHandler = ui.getErrorHandler();
                errorHandler.processError(LanguagePropertyType.ERROR_DATA_FILE_LOADING, "todo", "todo");
                System.exit(0);
            }
        });
        languageInput.setScene(new Scene(pane));
        languageInput.setMinHeight(300);
        languageInput.setMinWidth(500);
        languageInput.show();
    }

    /**
     * Loads this application's properties file, which has a number of settings
     * for initializing the user interface.
     *
     * @return true if the properties file was loaded successfully, false
     * otherwise.
     */
    public boolean loadProperties() {
        try {
            // LOAD THE SETTINGS FOR STARTING THE APP
            PropertiesManager props = PropertiesManager.getPropertiesManager();
            props.addProperty(PropertiesManager.DATA_PATH_PROPERTY, PATH_DATA);
            props.loadProperties(UI_PROPERTIES_FILE_NAME, PROPERTIES_SCHEMA_FILE_NAME);
            return true;
        } catch (InvalidXMLFileFormatException ixmlffe) {
            // SOMETHING WENT WRONG INITIALIZING THE XML FILE
            ErrorHandler eH = ui.getErrorHandler();
            eH.processError(LanguagePropertyType.ERROR_PROPERTIES_FILE_LOADING, "todo", "todo");
            return false;
        }
    }

    /**
     * This is where the application starts execution. We'll load the
     * application properties and then use them to build our user interface and
     * start the window in event handling mode. Once in that mode, all code
     * execution will happen in response to user requests.
     *
     * @param args This application does not use any command line arguments.
     */
    public static void main(String[] args) {
        launch(args);
    }
}
