package ssm.model;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import properties_manager.PropertiesManager;
import ssm.LanguagePropertyType;
import ssm.view.SlideShowMakerView;

/**
 * This class manages all the data associated with a slideshow.
 *
 * @author McKilla Gorilla & Rahul S Agasthya
 */
public class SlideShowModel {

    SlideShowMakerView ui;
    String title;
    ObservableList<Slide> slides;
    Slide selectedSlide;
    boolean isEdited;

    public SlideShowModel() {
    }

    public SlideShowModel(SlideShowMakerView initUI) {
        ui = initUI;
        slides = FXCollections.observableArrayList();
        reset();
    }

    // ACCESSOR METHODS
    public boolean isSlideSelected() {
        return selectedSlide != null;
    }

    public ObservableList<Slide> getSlides() {
        return slides;
    }

    public Slide getSelectedSlide() {
        return selectedSlide;
    }

    public String getTitle() {
        return title;
    }

    public boolean isEdited() {
        return isEdited;
    }

    // MUTATOR METHODS
    public void setSelectedSlide(Slide initSelectedSlide) {
        selectedSlide = initSelectedSlide;
    }

    public void setTitle(String initTitle) {
        title = initTitle;
    }

    public void setEdited(boolean initEdited) {
        isEdited = initEdited;
    }

    // SERVICE METHODS
    /**
     * Resets the slide show to have no slides and a default title.
     */
    public void reset() {
        slides.clear();
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        title = props.getProperty(LanguagePropertyType.DEFAULT_SLIDE_SHOW_TITLE);
        selectedSlide = null;
    }

    /**
     * Adds a slide to the slide show with the parameter settings.
     *
     * @param initImageFileName File name of the slide image to add.
     * @param initImagePath File path for the slide image to add.
     */
    public void addSlide(String initImageFileName,
            String initImagePath,
            String initCaption) {
        Slide slideToAdd = new Slide(initImageFileName, initImagePath);
        slideToAdd.setCaption(initCaption);
        slides.add(slideToAdd);
        ui.reloadSlideShowPane(this);
    }

    /**
     * Removes a slide from the show.
     */
    public void removeSlide() {
        int index = slides.indexOf(selectedSlide);
        boolean a = slides.remove(selectedSlide);
        ui.reloadSlideShowPane(this);
    }

    /**
     * Moves the slide up the ObservableList.
     * 
     * On the GUI, it will move up the scroll pane.
     */
    public void moveUp() {
        int indexFrom = slides.indexOf(selectedSlide);
        int indexTo = indexFrom - 1;
        if (indexTo < 0) {
            return;
        }
        Slide temp = slides.get(indexFrom);
        slides.set(indexFrom, slides.get(indexTo));
        slides.set(indexTo, temp);
        ui.reloadSlideShowPane(this);
    }

    /**
     * Moves the slide down the ObservableList.
     * 
     * On the GUI, it will move down the scroll pane.
     */
    public void moveDown() {
        int indexFrom = slides.indexOf(selectedSlide);
        int indexTo = indexFrom + 1;
        if (indexTo >= slides.size()) {
            return;
        }
        Slide temp = slides.get(indexFrom);
        slides.set(indexFrom, slides.get(indexTo));
        slides.set(indexTo, temp);
        ui.reloadSlideShowPane(this);
    }

    /**
     * Gives the size of the ObservableList.
     * @return slides.size()
     */
    public int size() {
        return slides.size();
    }
}
