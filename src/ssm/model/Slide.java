package ssm.model;

/**
 * This class represents a single slide in a slide show.
 *
 * @author McKilla Gorilla & _____________
 */
public class Slide {

    String imageFileName;
    String imagePath;
    String caption;
    Boolean isSelected;

    /**
     * Constructor, it initializes all slide data.
     *
     * @param initImageFileName File name of the image.
     *
     * @param initImagePath File path for the image.
     *
     * Sets isSelected to false.
     */
    public Slide(String initImageFileName, String initImagePath) {
        imageFileName = initImageFileName;
        imagePath = initImagePath;
        isSelected = false;
    }

    // ACCESSOR METHODS
    public String getImageFileName() {
        return imageFileName;
    }

    public String getImagePath() {
        return imagePath;
    }

    public String getCaption() {
        return caption;
    }

    public Boolean isSelected() {
        return isSelected;
    }

    // MUTATOR METHODS
    public void setImageFileName(String initImageFileName) {
        imageFileName = initImageFileName;
    }

    public void setImagePath(String initImagePath) {
        imagePath = initImagePath;
    }

    public void setImage(String initPath, String initFileName) {
        imagePath = initPath;
        imageFileName = initFileName;
    }

    public void setCaption(String initCaption) {
        caption = initCaption;
    }

    public void setSelected(Boolean initSelected) {
        isSelected = initSelected;
    }
}
